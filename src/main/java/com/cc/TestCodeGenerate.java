package com.cc;

import com.cc.util.CodeGenerate;

/**
 * @author CC
 * @date Created in 2023/7/7 21:54
 */
public class TestCodeGenerate {
    public static void main(String[] args) {
        //生成    纯字母         的6位验证码
        System.out.println(CodeGenerate.codeGenerate(6, true, false, false));
        //生成    纯数字         的6位验证码
        System.out.println(CodeGenerate.codeGenerate(6, false, true, false));
        //生成    数字+字母       的6位验证码
        System.out.println(CodeGenerate.codeGenerate(6));
        //生成    混合模式验证码   的6位验证码
        System.out.println(CodeGenerate.codeGenerate(6, true, true, true));
        //生成    特殊字符+字母    的6位验证码
        System.out.println(CodeGenerate.codeGenerate(6, true, false, true));
        //生成    特殊字符+数字    的6位验证码
        System.out.println(CodeGenerate.codeGenerate(6, false, true, true));
        //生成    计算题类型验证码
        System.out.println(CodeGenerate.mathCode());
        //获取计算题类型验证码答案
        System.out.println(CodeGenerate.getMathCodeAnswer());
        //自定义secret来生成验证码
        System.out.println(CodeGenerate.diyCodeSecret("1352daloGaz", 5));
    }
}
