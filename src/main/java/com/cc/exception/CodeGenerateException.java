package com.cc.exception;

/**
 * @author CC
 * @date Created in 2023/7/7 21:20
 */
public class CodeGenerateException extends Exception{
    public CodeGenerateException(String message) {
        super(message);
    }
}
