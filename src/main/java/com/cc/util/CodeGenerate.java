package com.cc.util;

import com.cc.exception.CodeGenerateException;

import java.util.Random;

/**
 * @author CC
 * @date Created in 2023/7/7 20:14
 */
public class CodeGenerate {
    //基础验证码的范围
    public static final String baseSecret = "qwertyuiopasdfghjklzxcvbnm";
    public static String letterSecret;//字符的验证码取值范围
    public static String numberSecret = "1234567890";//数字的验证码取值范围
    public static String specialWord = "@#$&";//特殊字符
    public static String mixSecret;//混合模式验证码
    public static Random random;
    public static Integer mathAnswer;//获取计算题验证码答案
    public static String computedOperator = "+-*/";//计算符号

    static {
        letterSecret = baseSecret + baseSecret.toUpperCase();
        mixSecret = letterSecret + numberSecret + specialWord;
        random = new Random();
    }

    //可以指定生成验证码,包含字符,数字,特殊字符
    public static String codeGenerate(Integer codeLength, Boolean isNeedLetter, Boolean isNeedNumber, Boolean isNeedSpecialWords) {
        if (isNeedLetter && isNeedNumber && isNeedSpecialWords) {
            return codeUtils(generateNewSecret(mixSecret), codeLength);
        } else if (isNeedLetter && isNeedNumber) {
            //默认返回就是包含字母和数字的验证码
            return codeUtils(generateNewSecret(letterSecret.concat(numberSecret)), codeLength);
        } else if (isNeedLetter && isNeedSpecialWords) {
            return codeUtils(generateNewSecret(letterSecret.concat(specialWord)), codeLength);
        } else if (isNeedNumber && isNeedSpecialWords) {
            return codeUtils(generateNewSecret(numberSecret.concat(specialWord)), codeLength);
        } else if (isNeedNumber) {
            return codeUtils(generateNewSecret(numberSecret), codeLength);
        } else if (isNeedSpecialWords) {
            //只包含特殊字符的情况
            try {
                throw new CodeGenerateException("验证码不能只包含特殊字符");
            } catch (CodeGenerateException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            //默认返回纯字母
            return codeUtils(generateNewSecret(letterSecret), codeLength);
        }
    }

    //只生成数字+字母的验证码
    public static String codeGenerate(Integer codeLength) {
        //默认返回就是包含字母和数字的验证码
        return codeUtils(generateNewSecret(letterSecret.concat(numberSecret)), codeLength);
    }

    //用于打乱secret顺序
    public static String generateNewSecret(String secret) {
        StringBuilder builder = new StringBuilder();
        StringBuilder secretBuilder = new StringBuilder(secret);
        for (int i = 0; i < secret.length(); i++) {
            int index = random.nextInt(secretBuilder.length());
            builder.append(secretBuilder.charAt(index));
            secretBuilder.delete(index, index + 1);
        }
        return builder.toString();
    }

    //生成随机验证码
    public static String codeUtils(String secret, Integer length) {
        StringBuilder randomCode = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(secret.length());
            randomCode.append(secret.charAt(index));
        }
        return randomCode.toString();
    }

    //生成数字加减法验证码
    public static String mathCode() {
        int num1 = random.nextInt(50);
        int num2 = random.nextInt(60);
        int operator = random.nextInt(computedOperator.length());
        String operatorFc = computedOperator.substring(operator, operator + 1);
        mathAnswer = computeCode(operatorFc, num1, num2);
        return num1 + operatorFc + num2 + "=" + "?";
    }

    //获取数字加减法验证码的答案
    public static Integer getMathCodeAnswer() {
        return mathAnswer;
    }

    //计算提验证码值
    public static Integer computeCode(String operator, Integer num1, Integer num2) {
        switch (operator) {
            case "+":
                return num1 + num2;
            case "-":
                return num1 - num2;
            case "*":
                return num1 * num2;
            default:
                //0不做被除数
                if (num2 == 0) num2 += 1;
                return num1 / num2;
        }
    }

    //自定义secret生成验证码
    public static String diyCodeSecret(String secret, Integer length) {
        return codeUtils(secret, length);
    }
}
